package kosuhina.tm.controller;

import kosuhina.tm.entity.Project;
import kosuhina.tm.service.ProjectService;
import kosuhina.tm.service.SessionService;
import java.util.Collections;
import java.util.List;

public class ProjectController extends AbstractController{

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

     /**
     * Создание проекта
     * @return код выполнения
     */

    public int createProject(){
        System.out.println("[СОЗДАНИЕ ПРОЕКТА]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ПРОЕКТА:");
        final String name = scanner.nextLine();
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ОПИСАНИЕ ПРОЕКТА:");
        final String description = scanner.nextLine();
        projectService.create(name,description);
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByName(){
        System.out.println("[УДАЛЕНИЕ ПРОЕКТА]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ПРОЕКТА:");
        final String name = scanner.nextLine();
        final List<Project> projects  = projectService.removeByName(name);
        if (projects == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectById(){
        System.out.println("[УДАЛЕНИЕ ПРОЕКТА]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ID ПРОЕКТА:");
        final long id = scanner.nextLong();
        final Project project  = projectService.removeById(id);
        if (project == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByIndex(){
        System.out.println("[УДАЛЕНИЕ ПРОЕКТА]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИНДЕКС ПРОЕКТА :");
        final int index = scanner.nextInt() - 1;
        final Project project  = projectService.removeByIndex(index);
        if (project == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int updateProjectByIndex(){
        System.out.println("[ИЗМЕНЕНИЕ ПРОЕКТА]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИНДЕКС ПРОЕКТА:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project  = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[ОШИБКА]");
            return 0;
        }
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ПРОЕКТА:");
        final String name = scanner.nextLine();
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ОПИСАНИЕ ПРОЕКТА:");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int clearProject(){
        System.out.println("[УДАЛЕНИЕ ВСЕХ ПРОЕКТОВ]");
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int listProject(){
        System.out.println("[СПИСОК ПРОЕКТОВ]");
        int index = 1;
        viewProjects(projectService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[ДАННЫЕ ПРОЕКТА]");
        System.out.println("ID: " + project.getId());
        System.out.println("ИМЯ: " + project.getName());
        System.out.println("ОПИСАНИЕ: " + project.getDescription());
        System.out.println("[OK]");
    }

    public int viewProjectByIndex() {
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ПРОЕКТА:");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.findByIndex(index);
        viewProject(project);
        return 0;
    }

    public int addProjectToUserByIndex() {
        System.out.println("ДОБАВЛЕНИЕ ПРОЕКТА ТЕКУЩЕМУ ПОЛЬЗОВАТЕЛЮ");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИНДЕКС ПРОЕКТА:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project =projectService.findByIndexd(index);
        final long userId = SessionService.getInstance().getSessionId();
        project.setUserId(userId);
        System.out.println("[OK]");
        return 0;
    }

    public int listPojectProfile(){
        System.out.println("СПИСОК ПРОЕКТОВ ТЕКУЩЕГО ПОЛЬЗОВАТЕЛЯ: ");
        final Long id = SessionService.getInstance().getSessionId();
        final List<Project> projects = projectService.findAllByUserId(id);
        viewProjects(projects);
        System.out.println("[OK]");
        return 0;
    }

    public void viewProjects(final List<Project> projects) {
        if (projects == null || projects.isEmpty()) return;
        Collections.sort(projects);
        int index = 1;
            for (final Project project: projectService.findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
    }
}
