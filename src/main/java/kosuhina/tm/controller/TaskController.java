package kosuhina.tm.controller;

import kosuhina.tm.entity.Project;
import kosuhina.tm.entity.Task;
import kosuhina.tm.service.ProjectTaskService;
import kosuhina.tm.service.SessionService;
import kosuhina.tm.service.TaskService;
import java.util.*;

public class TaskController extends AbstractController{

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    public int removeTaskByName(){
        System.out.println("[УДАЛЕНИЕ ЗАДАЧИ ПО ИМЕНИ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ЗАДАЧИ:");
        final String name = scanner.nextLine();
        final List <Task> tasks  = taskService.removeByName(name);
        if (tasks == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskById(){
        System.out.println("[УДАЛЕНИЕ ЗАДАЧИ ПО ID]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ID ЗАДАЧИ:");
        final long id = scanner.nextLong();
        final Task task  = taskService.removeById(id);
        if (task == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex(){
        System.out.println("[УДАЛЕНИЕ ЗАДАЧИ ПО ИНДЕКСУ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИНДЕКС ЗАДАЧИ:");
        final int id = scanner.nextInt() - 1;
        final Task task  = taskService.removeByIndex(id);
        if (task == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex(){
        System.out.println("[ИЗМЕНЕНИЕ ЗАДАЧИ]");
        System.out.println("ПОЖАЛУЙСТА, ВНЕСИТЕ ИНДЕКС ЗАДАЧИ:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task  = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[ОШИБКА]");
            return 0;
        }
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ЗАДАЧИ:");
        final String name = scanner.nextLine();
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ОПИСАНИЕ ЗАДАЧИ:");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int createTask(){
        System.out.println("[СОЗДАНИЕ ЗАДАЧИ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ЗАДАЧИ:");
        final String name = scanner.nextLine();
        taskService.create(name);
        System.out.println("[OK]");
        return 0;
    }

    public int listTaskProfile(){
        System.out.println("СПИСОК ЗАДАЧ ТЕКУЩЕГО ПОЛЬЗОВАТЕЛЯ: ");
        final Long id = SessionService.getInstance().getSessionId();
        final List<Task> tasks = taskService.findAllByUserId(id);
        viewTasks(tasks);
        System.out.println("[OK]");
        return 0;
    }

    public int clearTask(){
        System.out.println("[УДАЛЕНИЕ ЗАДАЧ]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[ПРОСМОТР ЗАДАЧИ]");
        System.out.println("ID ПОЛЬЗОВАТЕЛЯ: " + task.getUserId());
        System.out.println("ID: " + task.getId());
        System.out.println("ИМЯ: " + task.getName());
        System.out.println("ОПИСАНИЕ: " + task.getDescription());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex() {
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ЗАДАЧИ:");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.findByIndex(index);
        viewTask(task);
        return 0;
    }

    public int listTask(){
        System.out.println("[СПИСОК ЗАДАЧ]");
        int index = 1;
        viewTasks(taskService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    public void viewTasks (final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        Collections.sort(tasks);
        int index = 1;
        for (final Task task: taskService.findAll()) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
    }

    public int addTaskToProjectByIds() {
        System.out.println("ДОБАВЛЕНИЕ ЗАДАЧИ В ПРОЕКТ ПО ID");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ID ПРОЕКТА:");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ID ЗАДАЧИ:");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.addTaskToProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskToProjectByIds() {
        System.out.println("УДАЛЕНИЕ ЗАДАЧИ ИЗ ПРОЕКТА ПО ID");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ID ПРОЕКТА:");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("ПОЖАЛУСТА, ВВЕДИТЕ ID ЗАДАЧИ:");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int addTaskToUserByIndex() {
        final Long id = SessionService.getInstance().getSessionId();
        if(id == null){
            System.out.println("ТЕКУЩИЙ ПОЛЬЗОВАТЕЛЬ НЕ ЗАДАН");
            System.out.println("ID: " + id);
            return 0;
        }
        System.out.println("НАЗНАЧЕНИЕ ЗАДАЧИ ТЕКУЩЕМУ ПОЛЬЗОВАТЕЛЮ ПО ИНДЕКСУ ЗАДАЧИ");
        System.out.println("ПОЖАЛУЙСТА, ВНЕСИТЕ ИНДЕКС ЗАДАЧИ:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        taskService.addTaskToUser(id, index);
        System.out.println("[OK]");
        return 0;
    }

}
