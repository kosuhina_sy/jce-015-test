package kosuhina.tm;

import kosuhina.tm.controller.*;
import kosuhina.tm.enumerated.Role;
import kosuhina.tm.repository.ProjectRepository;
import kosuhina.tm.repository.TaskRepository;
import kosuhina.tm.repository.UserRepository;
import kosuhina.tm.service.*;
import java.util.Scanner;

import static kosuhina.tm.constant.TerminalConst.*;

/**
 * Основной класс
 */
public class Application {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final UserRepository userRepository = new UserRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final UserService userService = new UserService(userRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskController taskController = new TaskController(taskService, projectTaskService);

    private final SystemController systemController = new SystemController();

    private final UserController userController = new UserController(userService);

    {
        userService.create("test", "test", Role.USER);
        userService.create("admin", "admin", Role.ADMIN);
        projectRepository.create("DEMO3 PROJECT 1");
        projectRepository.create("DEMO9 PROJECT 2");
        projectRepository.create("DEMO1 PROJECT 3");
        taskRepository.create("TEST5");
        taskRepository.create("TEST3");
        taskRepository.create("TEST9");
        taskRepository.create("TEST7");

    }

    /**
     * Точка входа
     * @param args дополнительные аргументы запуска приложения
     */

    public static void main(final String[] args) {
        final Application application = new Application();
        final Scanner scanner = new Scanner(System.in);
        application.run(args);
        application.displayWelcome();
       String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            application.run(command);
        }
    }



    public void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * Обработка консольного ввода
     * @param param
     * @return код выполнения
     */


    private int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        CommandController.getHistory().add(param);
        switch (param) {
            case VERSION: return systemController.displayVersion();
            case ABOUT: return systemController.displayAbout();
            case HELP: return displayHelp();
            case EXIT: return systemController.displayExit();
            case COMMAND_HISTORY :return systemController.displayCommandHistory();

            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_CREATE: return projectController.createProject();
            case PROJECT_VIEW: return projectController.viewProjectByIndex();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();
            case PROJECT_ADD_TO_USER_BY_INDEX: return projectController.addProjectToUserByIndex();
            case PROJECT_LIST_PROFILE: return projectController.listPojectProfile();

            case TASK_LIST: return taskController.listTask();
            case TASK_CLEAR: return taskController.clearTask();
            case TASK_CREATE: return taskController.createTask();
            case TASK_VIEW: return taskController.viewTaskByIndex();
            case TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
            case TASK_REMOVE_BY_ID: return taskController.removeTaskById();
            case TASK_REMOVE_BY_INDEX: return taskController.removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX: return taskController.updateTaskByIndex();
            case TASK_ADD_TO_PROJECT_BY_IDS: return taskController.addTaskToProjectByIds();
            case TASK_REMOVE_FROM_PROJECT_BY_IDS: return taskController.removeTaskToProjectByIds();
            case TASK_ADD_TO_USER_BY_INDEX: return taskController.addTaskToUserByIndex();
            case TASK_LIST_PROFILE: return taskController.listTaskProfile();


            case USER_CREATE: return userController.createUser();
            case USER_LIST: return userController.listUser();
            case USER_CLEAR: return userController.clearUser();
            case USER_UPDATE_BY_ID: return userController.updateUserById();
            case USER_UPDATE_BY_INDEX: return userController.updateUserByIndex();
            case USER_UPDATE_BY_LOGIN: return userController.updateUserByLogin();
            case USER_REMOVE_BY_INDEX: return userController.removeUserByIndex();
            case USER_REMOVE_BY_ID: return userController.removeUserById();
            case USER_REMOVE_BY_LOGIN: return userController.removeUserByLogin();
            case USER_VIEW_BY_ID: return userController.viewUserById();
            case USER_VIEW_INDEX: return userController.viewUserByIndex();
            case USER_VIEW_BY_LOGIN: return userController.viewUserByLogin();
            case USER_LOGIN: return userController.loginUser();
            case USER_LOGOUT: return userController.logoutUser();
            case USER_VIEW_PROFILE: return userController.viewUserProfile();
            case USER_UPDATE_PROFILE: return userController.updateUserProfile();
            case USER_UPDATE_PASSWORD: return userController.updateProfilePassword();

            default: return systemController.displayError();
        }
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public int displayHelp() {
        System.out.println("version - Вывод на экран версии приложения.");
        System.out.println("about - Вывод на экран сведений о разработчике.");
        System.out.println("help - Вывод на экран списка терминальных команд.");
        System.out.println("command-history - Вывод на экран истории введенных терминальных команд.");
        System.out.println("exit - Выход из приложения.");
        System.out.println();
        System.out.println("project-list - Вывод на экран списка проектов.");
        System.out.println("project-create - Создание нового проекта по имени.");
        System.out.println("project-clear - Удаление всех проектов.");
        System.out.println("project-view -  Просмотр проекта по индексу.");
        System.out.println("project-remove-by-name - Удаление проекта по имени.");
        System.out.println("project-remove-by-id - Удаление проекта по id.");
        System.out.println("project-remove-by-index - Удаление проекта по индексу.");
        System.out.println("project-update-by-index - Изменение проекта по индексу.");
        System.out.println("project-add-to-user-by-index - Добавление проекта текущему пользователю по индексу проекта.");
        System.out.println("project-list-profile - Просмотр проектов текущего пользователя.");

        System.out.println();
        System.out.println("task-list - Вывод на экран списка задач.");
        System.out.println("task-create - Создание новой задачи по имени.");
        System.out.println("task-clear - Удаление всех задач.");
        System.out.println("task-view -  Просмотр задачи по индексу.");
        System.out.println("task-remove-by-name - Удаление задачи по имени.");
        System.out.println("task-remove-by-id - Удаление задачи по id.");
        System.out.println("task-remove-by-index - Удаление задачи по индексу.");
        System.out.println("task-update-by-index - Изменение задачи по индексу.");
        System.out.println("task-list-by-project-id - Вывод на экран списка задач по id проекта.");
        System.out.println("task-add-to-project-by-ids - Добавление задачи по id проектов.");
        System.out.println("task-remove-from-project-by-ids - Удаление задачи по id проектов.");
        System.out.println("task-add-to-user-by-index - Добавление задачи текущему пользователю по индексу задачи.");
        System.out.println("task-list-profile - Просмотр задач текущего пользователя.");


        System.out.println();
        System.out.println("user-create - Создание нового пользователя.");
        System.out.println("user-list - Вывод на экран списка пользователей.");
        System.out.println("user-clear - Удаление всех пользователей");
        System.out.println("user-update-by-id - Изменение пользователя по id.");
        System.out.println("user-update-by-index -  Изменение пользователя по индексу.");
        System.out.println("user-update-by-login - Изменение пользователя по логину.");
        System.out.println("user-remove-by-id - Удаление пользователя по id.");
        System.out.println("user-remove-by-index - Удаление пользователя по индексу.");
        System.out.println("user-remove-by-login - Удаление пользователя по логину.");
        System.out.println("user-view-by-id - Просмотр пользователя по id.");
        System.out.println("user-view-by-index - Просмотр пользователя по индексу.");
        System.out.println("user-view-by-login - Просмотр пользователя по логину.");
        System.out.println("user-login - Аутентификация пользователя.");
        System.out.println("user-logout - Выход пользователя.");
        System.out.println("user-view-profile - Проcмотр профиля текущего пользователя.");
        System.out.println("user-update-profile - Изменение профиля текущего пользователя.");
        System.out.println("user-update-password - Изменение пароля текущего пользователя.");

        System.out.println();
        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

}
