package kosuhina.tm.entity;

import kosuhina.tm.Application;

public class Task implements Comparable<Task> {

    private Long id = System.nanoTime();

    private String name = "";

    private String description = "";

    private Long projectId;

    private Long userId;

    public Task(){

    }

    public Task(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public void setUserId(Long userId) { this.userId = userId; }

    public Long getUserId() { return userId; }

    @Override
    public String toString() {
        return id + ": " + name;
    }

    @Override
    public int compareTo(Task task) {
        int res = this.name.compareTo(task.name);
        return res;
    }

}
